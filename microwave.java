public class microwave {
	public double price;
	public String brand;
	public int wattage;

	public void sayPrice(double price) {
		System.out.println("I cost: " + price + " $");
	}

	public void makePopcorn(){
		System.out.println("*pop* *pop* *pop* *beep*  *beep* *beep*");
		System.out.println("Enjoy your popcorn");
	}
	
	public void heatForbiddenSoup() {
		System.out.println("mmmmmm mmmmmmm mmmmmm *beep* *beep* *beep*");
		System.out.println("Enjoy your forbidden soup!");
	}
}