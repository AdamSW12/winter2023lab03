import java.util.Scanner;
public class ApplianceStore {
	public static void main(String[] args) {
		microwave[] microwaves = new microwave[4];
		Scanner scan = new Scanner(System.in);
		
		for(int i = 0; i < microwaves.length; i++){
			microwaves[i] = new microwave();
			
			System.out.println("enter the price of microwave " + i);
			microwaves[i].price = scan.nextDouble();
			
			System.out.println("enter the wattage of microwave " + i);
			microwaves[i].wattage = scan.nextInt();
			scan.nextLine();
			System.out.println("enter the brand of microwave " + i);
			microwaves[i].brand = scan.nextLine();
			
			System.out.println(); //line to make it pretty
		}
		
		System.out.println("Microwave 4 costs: " + microwaves[3].price + " It's brand is: " + microwaves[3].brand + " and its wattage is: " + microwaves[3].wattage);
		
		System.out.println(); //line to make it pretty 
		
		microwaves[0].sayPrice(microwaves[0].price);
		microwaves[0].makePopcorn();
		microwaves[0].heatForbiddenSoup();
		
	}
}